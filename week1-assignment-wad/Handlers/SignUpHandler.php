<?php
session_start();
//To redirect to another page when the login/signup has been successful
function RedirectToURL($url, $waitmsg = 0.4)
{
    header("Refresh:$waitmsg; URL= $url");
    exit;
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
} $err= "" ;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // The request is using the POST method

    if (empty($_POST['signup_email'])){
        $err = "The email field is empty";
    }else{
        $signup_email = test_input($_POST['signup_email']);
    }

    if (empty($_POST['signup_psw'])) {
        $err = "The password fields is empty";
    }else{
        $signup_psw = $_POST['signup_psw'];
        $signup_psw = PASSWORD_HASH($signup_psw,PASSWORD_DEFAULT);
    }

}


function validateform()
{


    $GLOBALS['err'] = "";
    //Make sure that when the email is not correct it will adjust the $err value
    if (!filter_var($GLOBALS["signup_email"], FILTER_VALIDATE_EMAIL))
    {
        $err = "Invalid email";
    }
    //Make sure that when the password is not 6 characters, the $err will change
    if(strlen($_POST['signup_psw'])<6){
        $err = "Short password";
    }
    //Then in the last block it check if the error didn't be set, it does not allow the input to be registered on server.
    if (isset($err) != true) {
        return "validation check";
    } else {
        return $err;
    }
}




// add the PDO connection
include "data.php";


if($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (validateform() === "Short password") {
        //If the password was too short. A message will show and the user will be redirected to the signup page
        echo('<script>alert("Your password is insecure. It should be 6 characters or more!");</script>');
        RedirectToURL("../php/index.php?page=signup", 0);
    }
    if (validateform() === "Invalid email") {
        //if the email was not valid. A message will show and the user will be redirected to the signup page
        echo('<script>alert("The entered email, is not in the correct format!");</script>');
        RedirectToURL("../php/index.php?page=signup", 0);
    }
    $email = $_POST['signup_email'];
    $password = $_POST['signup_psw'];
    $position = "user";
    if(validateform() === "validation check") {


        //If the password and email passed the validation then: Execute the sql statement
        $nrows = $pdo->exec("INSERT INTO users(email,password,position) VALUES ('$email', '$password', '$position')");
        if ($nrows == 1) {
            //echo "Login Credentials verified";
            echo "<div class='container' ><div class='jumbotron' align='middle' >
                    <h1>Dear Friend, you have been successfully registered in our sites.$email!</h1>
                    <br> Incase you need to contact us,<br> Feel free to send us an email at:<br>
                    nourmagdalawi1@gmail.com <strong>SiNoTech Admins</strong>';</div></div>";
            //Make sure the user wil be redirected to the login page so the user can log in
            RedirectToURL("../php/index.php?page=login", 6);
            //}
        } else {
            //echo "Invalid Login Credentials"
            echo "<script type='text/javascript'>alert('Invalid sign up Credentials')</script>";
        }
    }
}



?>