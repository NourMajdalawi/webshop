<?php
// add the PDO connection


include "data.php";

$sql = 'SELECT * FROM product WHERE category = "laptop"';
$sth = $pdo->prepare($sql);
$sth->execute();
$result = $sth->fetchAll();
if($result !== false ){
    $products = array();
    foreach ( $result as $item ) {
        $item = <<<EOT
<div class="item">
			<div class="product-image"><img src="$item[2]" style="display: grid" width="220" height="170"
			                                alt="$item[1]"/></div>
			<h2 class="product-name">$item[1]</h2>
			<div class="description">
				<ul>
					<li> $item[4]</li>
					<li> $item[5] </li>
					<li>$item[6]</li>
					<li> $item[7]</li>
				</ul>
			</div>
			<div class="price-box"><span class="price" id="product-price-5">€$item[3]</span></div>
			<div class="actions">

				<input type="submit" name="sP" value="Add to Basket">
			</div>
		</div>
    
EOT;
        array_push($products, $item);
    }

}
$_SESSION['products'] = $products;


?>