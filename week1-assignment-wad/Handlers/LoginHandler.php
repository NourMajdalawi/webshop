<?php
session_start();
//To redirect to another page when the login/signup has been successful
function RedirectToURL($url, $waitmsg = 0.4)
{
    header("Refresh:$waitmsg; URL= $url");
    exit;
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
} $err= "" ;


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // The request is using the POST method

    if (empty($_POST['login_email'])){
        $err = "The email field is empty";
    }else{
        $signup_email = test_input($_POST['login_email']);
    }

    if (empty($_POST['login_password'])) {
        $err = "The password fields is empty";
    }else{
        $signup_psw = $_POST['login_password'];
        $signup_psw = PASSWORD_HASH($signup_psw,PASSWORD_DEFAULT);
    }

}


// add the PDO connection
include "data.php";

//Set values to the variables
if (isset($_POST['login_email']) and isset($_POST['login_password'])) {
    $email = $_POST['login_email'];
    $password = $_POST['login_password'];

    $sql = 'SELECT * FROM users WHERE email=:email AND  password=:password';
    $sth = $pdo->prepare($sql);
    $sth->execute([':email' => $email, ':password' => $password]);
    $result = $sth->fetch();

    if($result[1] === $email && $result[2] === $password){
        echo "<div class='container'  ><div class='jumbotron' align='middle' ><h1>Hi, $email!</h1><h2>log in succeed</h2>
                                  </div></div>";
        if($result[3] === "admin"){
            $_SESSION['admin']=$result[3];
        }

        /*if the email is exist and everything is okey, set the session
	    to use it in the memberpage => hello <email>;*/

        $_SESSION['email']=$email;
        RedirectToURL("../php/index.php?page=home", 2);
    }
    else{
        echo "<script type='text/javascript'>alert('Invalid Login Credentials')</script>";
        RedirectToURL("../php/index.php?page=login", 0);
    }
}
?>