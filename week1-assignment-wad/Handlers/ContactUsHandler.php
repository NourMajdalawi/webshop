<?php
session_start();
function RedirectToURL($url, $waitmsg = 0.4)
{
    header("Refresh:$waitmsg; URL= $url");
    exit;
}
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
$err = "" ;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // The request is using the POST method

    if (empty($_POST['firstname'])){
        $err = "The firstname field is empty";
    }else{
        $fname = test_input($_POST['firstname']);
    }
    if (empty($_POST['lastname'])) {
        $err = "The lastname fields is empty";
    }else{
        $lname = $_POST['lastname'];
    }
    if (empty($_POST['Email'])) {
        $err = "The email fields is empty";
    }else{
        $email = $_POST['Email'];
    }
    if (empty($_POST['issue'])) {
        $err = "The issue fields is empty";
    }else{
        $issue = $_POST['issue'];
    }
    if (empty($_POST['Telephone'])) {
        $err = "The telephone fields is empty";
    }else{
        $phone = $_POST['Telephone'];
    }
    if (empty($_POST['message'])) {
        $err = "The message fields is empty";
    }else{
        $message = $_POST['message'];
    }

}

function validateform()
{
    $GLOBALS['err'] = "";
    if(empty($GLOBALS['fname']) ){
        $err = "Fname is empty";
    }elseif (!preg_match("/^[a-zA-Z ]*$/", $GLOBALS['fname'])) {
        $err = "fname format";
    }
    if(empty($GLOBALS['lname']) ){
        $err = "Lname is empty";
    }elseif (!preg_match("/^[a-zA-Z ]*$/", $GLOBALS['lname'])) {
        $err = "Lname format";
    }
    if(empty($GLOBALS['email']) ){
        $err = "Email is empty";
    }elseif (!filter_var($GLOBALS['email'],FILTER_VALIDATE_EMAIL)) {
        $err = "Email format";
    }
    if(empty($GLOBALS['phone'])){
        $err = "Phone is empty";
    }
    if($GLOBALS['issue']==="-1"){
        $err ="Issue is empty";
    }
    if(empty($GLOBALS['message'])){
        $err ="Message is empty";
    }
    if (isset($err) != true) {
        return "validation check";
    } else {
        return $err;
    }
}
//set the connection to the database
include "data.php";
//Make sure the user gets a message when a field is empty or not in the correct format
if($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (validateform() === "Fname is empty") {
        echo('<script>alert("First name field is empty");</script>');
    }
    if (validateform()==="fname format"){
        echo ('<script>alert("First name not in the correct format");</script>');
    }
    if (validateform() === "Lname is empty") {
        echo('<script>alert("Last name field is empty");</script>');
    }
    if (validateform()==="lname format"){
        echo ('<script>alert("Last name not in the correct format");</script>');
    }
    if (validateform() === "Email is empty") {
        echo('<script>alert("Email needs to be filled in");</script>');
    }
    if (validateform()==="Email format"){
        echo ('<script>alert("Email is not in the correct format");</script>');
    }
    if (validateform() === "Phone is empty") {
        echo('<script>alert("Phone number needs to be filled in");</script>');
    }
    if (validateform() === "Issue is empty") {
        echo('<script>alert("Issue needs to be filled in");</script>');
    }
    if (validateform() === "Message is empty") {
        echo('<script>alert("Please fill in the message field");</script>');
    }

    $fname = $_POST['firstname'];
    $lname = $_POST['lastname'];
    $email = $_POST['Email'];
    $message = $_POST['message'];
    $phone = $_POST['Telephone'];
    $issue=$_POST['issue'];

    if(validateform() === "validation check"){
        $nrows = $pdo->exec("INSERT INTO contactusers(fisrtname, lastname, email, telephone, subject, massege) VALUES ('$fname', '$lname', '$email', '$phone', '$issue', '$message')");
        //echo "Invalid contact information"
        echo "<script type='text/javascript'>alert('Your request has been successfully submitted')</script>";
        RedirectToURL("../php/index.php?page=home", 2);
    }
    else {
        //echo "Invalid contact information"
        echo "<script type='text/javascript'>alert('Invalid contact information')</script>";
        RedirectToURL("../php/index.php?page=AboutUsAndContactUs", 2);
    }

}
?>

