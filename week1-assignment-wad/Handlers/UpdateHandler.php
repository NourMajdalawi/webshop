<?php
// Define variables and initialize with empty values
$name = $image = $price = $info1 = $info2 = $info3 = $info4 = $category = "";
$name_err = $image_err = $price_err = $info1_err = $info2_err = $info3_err = $info4_err = $category_err= "";


// Processing form data when form is submitted
if(isset($_POST["id"]) && !empty($_POST["id"])) {
    // Get hidden input value
    $id = $_POST["id"];

    // Validate name
    $input_name = trim($_POST["name"]);
    if (empty($input_name)) {
        $name_err = "Please enter a name.";
    } elseif (!filter_var($input_name, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[a-zA-Z\s]+$/")))) {
        $name_err = "Please enter a valid name.";
    } else {
        $name = $input_name;
    }

    // Validate price
    $input_price = trim($_POST["price"]);
    if (empty($input_price)) {
        $price_err = "Please enter a price.";
    } elseif (!ctype_digit($input_price)) {
        $salary_err = "Please enter a positive integer value.";
    } else {
        $price = $input_price;
    }
    // Validate info1
    $input_info1 = trim($_POST["info1"]);
    if (empty($input_info1)) {
        $info1_err = "Please enter the first information.";
    } else {
        $info1 = $input_info1;
    }

    // Validate info2
    $input_info2 = trim($_POST["info2"]);
    if (empty($input_info2)) {
        $info2_err = "Please enter the second information.";
    } else {
        $info2 = $input_info2;
    }
    // Validate info3
    $input_info3 = trim($_POST["info3"]);
    if (empty($input_info3)) {
        $info3_err = "Please enter the third information.";
    } else {
        $info3 = $input_info3;
    }
    // Validate info4
    $input_info4 = trim($_POST["info4"]);
    if (empty($input_info4)) {
        $info4_err = "Please enter the fourth information.";
    } else {
        $info4 = $input_info4;
    }

    // Validate category
    $input_category = trim($_POST["category"]);
    if (empty($input_category)) {
        $category_err = "Please enter the category.";
    } else {
        $category = $input_category;
    }

}

else{
    // Check existence of id parameter before processing further
    if(isset($_GET["id"]) && !empty(trim($_GET["id"]))){
        // Get URL parameter
        $id =  trim($_GET["id"]);

        // Prepare a select statement
        include "../../Handlers/data.php";

        $sql = "SELECT * FROM product WHERE id = :id";
        if($stmt = $pdo->prepare($sql)){
            // Bind variables to the prepared statement as parameters
            $stmt->bindParam(":id", $param_id);

            // Set parameters
            $param_id = $id;

            // Attempt to execute the prepared statement
            if($stmt->execute()){
                if($stmt->rowCount() == 1){
                    /* Fetch result row as an associative array. Since the result set contains only one row, we don't need to use while loop */
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);

                    // Retrieve individual field value
                    $name = $row["name"];
                    $price = $row["price"];
                    $info1 = $row["info1"];
                    $info2 = $row["info2"];
                    $info3 = $row["info3"];
                    $info4 = $row["info4"];
                    $category = $row["category"];
                } else{
                    // URL doesn't contain valid id. Redirect to error page
                    echo "Something went wrong, try again later.";
                    exit();
                }

            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }

        // Close statement
        unset($stmt);

        // Close connection
        unset($pdo);
    }  else{
        // URL doesn't contain id parameter. Redirect to error page
        echo "Wrong, try again later!";
        exit();
    }
}

?>