<?php

class Product
{
    private $username = 'dbi439093';
    private $password = '12345';
    private $host = 'studmysql01.fhict.local';
    private $db = 'dbi439093';


    protected function createConnection()
    {
        try {
            // Create DB connection
            $pdo = new PDO("mysql:host=$this->host;dbname=$this->db", $this->username, $this->password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function CreateProduct()
    {

        function RedirectToURL($url, $waitmsg = 0.4)
        {
            header("Refresh:$waitmsg; URL= $url");
            exit;
        }

        // Define variables and initialize with empty values
        $name = $image = $price = $info1 = $info2 = $info3 = $info4 = $category = "";
        $name_err = $image_err = $price_err = $info1_err = $info2_err = $info3_err = $info4_err = $category_err = "";

        // Processing form data when form is submitted
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Validate name
            $input_name = trim($_POST["name"]);
            if (empty($input_name)) {
                $name_err = "Please enter a name.";
            } elseif (!filter_var($input_name, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[a-zA-Z\s]+$/")))) {
                $name_err = "Please enter a valid name.";
            } else {
                $name = $input_name;
            }

            // Validate price
            $input_price = trim($_POST["price"]);
            if (empty($input_price)) {
                $price_err = "Please enter a price.";
            } elseif (!ctype_digit($input_price)) {
                $salary_err = "Please enter a positive integer value.";
            } else {
                $price = $input_price;
            }
            // Validate info1
            $input_info1 = trim($_POST["info1"]);
            if (empty($input_info1)) {
                $info1_err = "Please enter the first information.";
            } else {
                $info1 = $input_info1;
            }

            // Validate info2
            $input_info2 = trim($_POST["info2"]);
            if (empty($input_info2)) {
                $info2_err = "Please enter the second information.";
            } else {
                $info2 = $input_info2;
            }
            // Validate info3
            $input_info3 = trim($_POST["info3"]);
            if (empty($input_info3)) {
                $info3_err = "Please enter the third information.";
            } else {
                $info3 = $input_info3;
            }
            // Validate info4
            $input_info4 = trim($_POST["info4"]);
            if (empty($input_info4)) {
                $info4_err = "Please enter the fourth information.";
            } else {
                $info4 = $input_info4;
            }

            // Validate category
            $input_category = trim($_POST["category"]);
            if (empty($input_category)) {
                $category_err = "Please enter the category.";
            } else {
                $category = $input_category;
            }

            // Check input errors before inserting in database
            if (empty($name_err) && empty($price_err) && empty($info1_err) && empty($info2_err) && empty($info4_err) && empty($info3_err) && empty($category_err)) {
                // Prepare an insert statement
                $pdo = $this->createConnection();
                $sql = "INSERT INTO product (name, price, info1, info2, info3, info4, category) VALUES (:name, :price, :info1, :info2, :info3, :info4, :category)";

                if ($stmt = $pdo->prepare($sql)) {
                    // Bind variables to the prepared statement as parameters
                    $stmt->bindParam(":name", $param_name);
                    $stmt->bindParam(":price", $param_price);
                    $stmt->bindParam(":info1", $param_info1);
                    $stmt->bindParam(":info2", $param_info2);
                    $stmt->bindParam(":info3", $param_info3);
                    $stmt->bindParam(":info4", $param_info4);
                    $stmt->bindParam(":category", $param_category);

                    // Set parameters
                    $param_name = $name;
                    $param_price = $price;
                    $param_info1 = $info1;
                    $param_info2 = $info2;
                    $param_info3 = $info3;
                    $param_info4 = $info4;
                    $param_category = $category;

                    // Attempt to execute the prepared statement
                    if ($stmt->execute()) {
                        // Records created successfully. Redirect to landing page
                        RedirectToURL("../index.php?page=admin", 0);
                        exit();
                    } else {
                        echo "Something went wrong. Please try again later.";
                    }
                }

                // Close statement
                unset($stmt);
            }

            // Close connection
            unset($pdo);
        }


    }

    public function ReadProduct()
    {
        // Attempt select query execution

        $pdo = $this->createConnection();
        $sql = 'SELECT * FROM product';
        $result = $pdo->query($sql);

        if ($result = $pdo->query($sql)) {
            if ($result->rowCount() > 0) {
                echo "<table class='products'>";
                echo "<thead>";
                echo "<tr>";
                echo "<th>ID</th>";
                echo "<th>Product name</th>";
                echo "<th>Product image</th>";
                echo "<th>Price</th>";
                echo "<th>Description</th>";
                echo "<th>Features</th>";
                echo "<th>Features</th>";
                echo "<th>Features</th>";
                echo "<th>category</th>";
                echo "<th>Action</th>";
                echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                while ($row = $result->fetch()) {
                    echo "<tr>";
                    echo "<td>" . $row['id'] . "</td>";
                    echo "<td>" . $row['name'] . "</td>";
                    echo "<td>" . $row['img'] . "</td>";
                    echo "<td>" . $row['price'] . "</td>";
                    echo "<td>" . $row['info1'] . "</td>";
                    echo "<td>" . $row['info2'] . "</td>";
                    echo "<td>" . $row['info3'] . "</td>";
                    echo "<td>" . $row['info4'] . "</td>";
                    echo "<td>" . $row['category'] . "</td>";
                    echo "<td>";

                    echo "<a href='Others/UpdateProduct.php?id=" . $row['id'] . "' title='Update product' data-toggle='tooltip'><span class='glyphicon glyphicon-pencil'></span></a>";
                    echo "<a href='Others/DeleteProduct.php?id=" . $row['id'] . "' title='Delete product' data-toggle='tooltip'><span class='glyphicon glyphicon-trash'></span></a>";
                    echo "</td>";
                    echo "</tr>";
                }
                echo "</tbody>";
                echo "</table>";
                // Free result set
                unset($result);
            } else {
                echo "<p class='lead'><em>No records were found.</em></p>";
            }
        } else {
            echo "ERROR: Could not able to execute $sql. ";
        }

// Close connection
        unset($pdo);
    }

    public function UpdateProduct()
    {

        function RedirectToURL($url, $waitmsg = 0.4)
        {
            header("Refresh:$waitmsg; URL= $url");
            exit;
        }

// Define variables and initialize with empty values
        $name = $image = $price = $info1 = $info2 = $info3 = $info4 = $category = "";
        $name_err = $image_err = $price_err = $info1_err = $info2_err = $info3_err = $info4_err = $category_err = "";


// Processing form data when form is submitted
        if (isset($_POST["id"]) && !empty($_POST["id"])) {
            // Get hidden input value
            $id = $_POST["id"];

            // Validate name
            $input_name = trim($_POST["name"]);
            if (empty($input_name)) {
                $name_err = "Please enter a name.";
            } elseif (!filter_var($input_name, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => "/^[a-zA-Z\s]+$/")))) {
                $name_err = "Please enter a valid name.";
            } else {
                $name = $input_name;
            }

            // Validate price
            $input_price = trim($_POST["price"]);
            if (empty($input_price)) {
                $price_err = "Please enter a price.";
            } elseif (!ctype_digit($input_price)) {
                $salary_err = "Please enter a positive integer value.";
            } else {
                $price = $input_price;
            }
            // Validate info1
            $input_info1 = trim($_POST["info1"]);
            if (empty($input_info1)) {
                $info1_err = "Please enter the first information.";
            } else {
                $info1 = $input_info1;
            }

            // Validate info2
            $input_info2 = trim($_POST["info2"]);
            if (empty($input_info2)) {
                $info2_err = "Please enter the second information.";
            } else {
                $info2 = $input_info2;
            }
            // Validate info3
            $input_info3 = trim($_POST["info3"]);
            if (empty($input_info3)) {
                $info3_err = "Please enter the third information.";
            } else {
                $info3 = $input_info3;
            }
            // Validate info4
            $input_info4 = trim($_POST["info4"]);
            if (empty($input_info4)) {
                $info4_err = "Please enter the fourth information.";
            } else {
                $info4 = $input_info4;
            }

            // Validate category
            $input_category = trim($_POST["category"]);
            if (empty($input_category)) {
                $category_err = "Please enter the category.";
            } else {
                $category = $input_category;
            }


            // Check input errors before inserting in database
            if (empty($name_err) && empty($price_err) && empty($info1_err) && empty($info2_err) && empty($info4_err) && empty($info3_err) && empty($category_err)) {
                // Prepare an insert statement
                $pdo = $this->createConnection();
                $sql = "UPDATE product SET name=:name, price=:price, info1=:info1, info2=:info2, info3=:info3, info4=:info4, category=:category WHERE id =:id";

                if ($stmt = $pdo->prepare($sql)) {
                    // Bind variables to the prepared statement as parameters
                    //$stmt->bindParam(":name", $param_name);
                    $stmt->bindParam(":id", $param_id);
                    $stmt->bindParam(":name", $param_name);
                    $stmt->bindParam(":price", $param_price);
                    $stmt->bindParam(":info1", $param_info1);
                    $stmt->bindParam(":info2", $param_info2);
                    $stmt->bindParam(":info3", $param_info3);
                    $stmt->bindParam(":info4", $param_info4);
                    $stmt->bindParam(":category", $param_category);

                    // Set parameters
                    $param_name = $name;
                    $param_price = $price;
                    $param_info1 = $info1;
                    $param_info2 = $info2;
                    $param_info3 = $info3;
                    $param_info4 = $info4;
                    $param_category = $category;
                    $param_id = $id;

                    // Attempt to execute the prepared statement
                    if ($stmt->execute()) {
                        // Records updated successfully. Redirect to landing page
                        RedirectToURL("../index.php?page=admin", 0);
                        exit();
                    } else {
                        echo "Something went wrong. Please try again later.";
                    }
                }

                // Close statement
                unset($stmt);
            }
            // Close connection
            unset($pdo);
        } else {
            // Check existence of id parameter before processing further
            if (isset($_GET["id"]) && !empty(trim($_GET["id"]))) {
                // Get URL parameter
                $id = trim($_GET["id"]);

                // Prepare a select statement
                $pdo = $this->createConnection();
                $sql = "SELECT * FROM product WHERE id = :id";
                if ($stmt = $pdo->prepare($sql)) {
                    // Bind variables to the prepared statement as parameters
                    $stmt->bindParam(":id", $param_id);

                    // Set parameters
                    $param_id = $id;

                    // Attempt to execute the prepared statement
                    if ($stmt->execute()) {
                        if ($stmt->rowCount() == 1) {
                            /* Fetch result row as an associative array. Since the result set contains only one row, we don't need to use while loop */
                            $row = $stmt->fetch(PDO::FETCH_ASSOC);

                            // Retrieve individual field value
                            $name = $row["name"];
                            $price = $row["price"];
                            $info1 = $row["info1"];
                            $info2 = $row["info2"];
                            $info3 = $row["info3"];
                            $info4 = $row["info4"];
                            $category = $row["category"];
                        } else {
                            // URL doesn't contain valid id. Redirect to error page
                            echo "Something went wrong, try again later.";
                            exit();
                        }

                    } else {
                        echo "Oops! Something went wrong. Please try again later.";
                    }
                }

                // Close statement
                unset($stmt);

                // Close connection
                unset($pdo);
            } else {
                // URL doesn't contain id parameter. Redirect to error page
                echo "Wrong, try again later!";
                exit();
            }
        }
    }

    public function DeleteProduct()
    {
        function RedirectToURL($url, $waitmsg = 0.4)
        {
            header("Refresh:$waitmsg; URL= $url");
            exit;
        }

// Process delete operation after confirmation
        if (isset($_POST["id"]) && !empty($_POST["id"])) {


            // Prepare a delete statement
            $pdo = $this->createConnection();
            $sql = "DELETE FROM product WHERE id = :id";

            if ($stmt = $pdo->prepare($sql)) {
                // Bind variables to the prepared statement as parameters
                $stmt->bindParam(":id", $param_id);

                // Set parameters
                $param_id = trim($_POST["id"]);

                // Attempt to execute the prepared statement
                if ($stmt->execute()) {
                    // Records deleted successfully. Redirect to landing page
                    RedirectToURL("../index.php?page=admin", 0);
                    exit();
                } else {
                    echo "Oops! Something went wrong. Please try again later.";
                }
            }

            // Close statement
            unset($stmt);

            // Close connection
            unset($pdo);
        } else {
            // Check existence of id parameter
            if (empty(trim($_GET["id"]))) {
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
    }

}

?>

