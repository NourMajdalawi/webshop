
<div class="aboutus" >
	<div class="aboutusinfo"><h3 align="middle">About Us</h3>
		<p>
            <h4>Who are we?</h4>
            Welcome to SinoTech, a web shop offering an enourmous range of technology based products. Our webshop offers mobile phones, laptops and more.
            The company was founded by Nour Magdalawi and Simone Geurtz, students of Fontys University of applied sciences ICT-software english stream.
            SinoTech has launched in february 2020.
            <br>

            <h4>Our aims</h4>
            By SiNoTech it is important that our customers are always satisfied and feel that they make
			the right choice by shopping online in our stores.  We always
            look forward getting new items and sell newer Products. We try to provide the best customer service possible and are always happy with new ideas.
            We hope to make every customer feel welcome. And it is our aim to provide every customer with the good they need.
            <br></p>

            <h4>Contact information</h4>

                <div class="grid-container2">
                    <div class="left">
                    <table>
                        <tr>
                            <td>Email:</td>
                            <td>nourmagdalawi1@gmail.com</td>
                        </tr>
                        <tr>
                            <td>Telephone:</td>
                            <td>0636342677</td>
                        </tr>
                        <tr>
                            <br>
                            <td>Adress:</td>
                            <td>Rachelsmolen 1 <br> 5612 MA <br> Eindhoven</td>
                        </tr>

                    </table>
                    </div>
                    <div class="right">

                    <div align="middle">
                    <button align="middle" id="HideMapBtn" onclick="HideMap()">Hide the map</button>
                    <button align="middle" id="ShowMapBtn" onclick="ShowMap()" style="display: none;">Show the map</button>
                    </div>
                    <div class="Gmap" align="middle">
                        <iframe id="Mapframe"
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2486.3302934806907!2d5.479793916212602!3d51.45209192256592!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c6d92199730073%3A0x1c98f3a0d8ee087b!2sFontys!5e0!3m2!1snl!2snl!4v1536619058336"
                                ></iframe>
                    </div>
                    </div>
                </div>
	</div>
</div>
