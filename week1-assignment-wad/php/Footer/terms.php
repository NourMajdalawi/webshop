<div class="termspage" align="middle">
    <div class="termsinfo"><h3>Terms and Conditions</h3>

    <div class="termstextheader"><h4>1. General:</h4>
    <div class="termstext"

        <p> 1. This site is runned by SinoTech, Rachelsmolen 1, 5612 MA, Eindhoven, The Netherlands hereafter "seller", "us", "ourselves" or "the company".
            "Client", "You" and "Your" refers to you, the person accessing this website and accepting the Company's terms and conditions. "Party", "Parties",
            or "Us", refers to both the Client and ourselves, or either the Client or ourselves. These terms and conditions apply to all offers, agreements and
            transactions between parties.</p>
    </div>


    <div class="termstextheader"><h4>2. Prices:</h4>
       <div class="termstext"

       <p>1. The recipient of an international shipment may be subject to such import taxes, customs duties and fees, which are levied once a shipment reaches your country. Additional charges for customs clearance must be borne by the recipient; we have no control over these charges and cannot predict what they may be. Shipments within the European Union are free of customs duties.</p>

        <p>2. We can change prices and conditions as long as no agreement is made between parties.</p>
    </div>


        <div class="termstextheader"><h4>3. Payment:</h4>
            <div class="termstext"

            <p>1.The clients uses one of the payment methods listed by us on the footer of the page.</p>

            <p>2. We only ship after we have received the full order amount.</p>
            <p>3. Orders can be cancelled by us when you do not pay for the orders. </p>
        </div>


        <div class="termstextheader"><h4>4. Shipping:</h4>
            <div class="termstext"
            <p>1. Client checks the received products for damage.</p>
            <p>2. When the received package or the products in the the package look damaged, the client will not use the damaged products and notify us about this damage within 7 days of receipt.</p>
        </div>


        <div class="termstextheader"><h4>5. Orders and agreements:</h4>
            <div class="termstext"
            <p>1. The client is responsible for supplying the correct information required to fulfil the clients needs and wishes. We assume the information supplied by the client is correct.</p>
            <p>
                2. We are allowed to deny orders from client without reason.

            </p>
              </div>


        <div class="termstextheader"><h4>6. Credit cards:</h4>
            <div class="termstext"
            <p>1. Client will not execute a credit card chargeback without contacting us in advance.</p>
            <p>2. When client executes a chargeback without a reason, with a false reason, or without contacting us in advance the amount of the chargeback will be converted into a debt. The debt needs to be payed by the client immediately. We hold the right to collect the debt, client will be held responsible for all costs involved.</p>
        </div>

        <div class="termstextheader"><h4>7. Products:</h4></div>
        <div class="termstext"
             <p>1. Displayed product images in any communication from us to client can differ from the real product.</p>
        </div>


    <div class="termstextheader"><h4>8. Force Majeure:</h4>
        <div class="termstext"
        <p>1. We will not be held responsible for any delay or failure to comply with our obligations under these conditions if the delay or failure arises from any cause which is beyond our reasonable control.</p>

    </div>
    <div class="termstextheader"><h4>9. Governing law and jurisdiction:</h4>
        <div class="termstext"
        <p>1. These conditions are governed by and construed in accordance with the laws of The Netherlands. You agree, as we do, to submit to the jurisdiction of the courts of The Netherlands.</p>
        <p>2. Application of the United Nations Convention on Contracts for the International Sale of Goods is expressly excluded for subjects covered by these conditions.
        </p>
    </div>

</div>
<div class="termstextheader"><h4>10. Limitation of liability:</h4>
    <div class="termstext"
    <p>1. The total liability of us for any claim or damage shall not exceed the price of the individual product whose defect or damage is the basis for claim. In no event shall we be liable for any loss of profit or for any other incidental or consequential damages.</p>
    <p>2. The user of a product knows the product can break down. When a product breaks down the user should immediately stop using it and disconnect the product from mains. The user reads the manual of a product carefully.
    </p>
    <p>3. When you are not the user, you inform the user about 10.2.</p>
    <p>4. We can not be held liable for damage or any claim arising from non-compliance with 10.3, 10.2, 5.1, 4.2, 4.1 by client or when the damage or claim is caused by negligent or intentional behaviour.</p>
</div>


    </div>

</div>
