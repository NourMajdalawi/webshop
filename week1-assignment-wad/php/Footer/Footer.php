 <footer>
	    <div class="footersec2">
		    <?php  include "../php/Footer/Payment.php"?>
            <div class="social">
	            <?php include "../php/Footer/SocialMedia.php"?>
            </div>
	    </div>

	    <div class="terms" align="middle">
		    <p><a href="?page=privacy">Privacy and policy</a> & <a href="?page=terms">Terms and conditions</a> </p>
	    </div>

	    <div>
		    <p align="middle" style="font-size: 12px;">This project started on
			    <?php date_default_timezone_set("Europe/Amsterdam");
			    $d = mktime(10,0,0,2,12,2020);
			    echo date('l, Y/m/d',$d);?>.</p>
		    <p align="middle" style="font-size: 12px;">&#x24B8;<?php date_default_timezone_set("Europe/Amsterdam");
			    echo date('Y');?> SinoTech Company. All rights Reserved.</p>
	    </div>
    </footer>
