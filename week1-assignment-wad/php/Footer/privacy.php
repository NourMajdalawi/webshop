<div class="privacypage" align="middle">
    <div class="privacyinfo"><h3>Privacy policy</h3>

<p class="privacytext">We value the privacy of visitors, customers and others who visit or deal with this store.</p>
        <div class="privacytextheader"><h4>General:</h4>
            <div class="privacytext"


            <p>1. Information of users collected by SinoTech is solely used by SinoTech to serve the users as good as possible.</p>
            <p>2. We do not sell obtained data to others. </p>
            <p>3. Only selected employees can access obtained user data in a secure environment.</p>
            <p>4. For security reasons, your ip-address (your unique internet address) will be saved with your order.</p>
        </div>

        <div class="privacytextheader"><h4>Payment information:</h4>
            <div class="privacytext"

            <p>1. All payments are handled by payment service providers.</p>

            <p>2. Credit card numbers are only stored on PSP's secure servers. </p>
            <p>3. Payment service providers can request additional information about you to process the payment.</p>
            </div>

        <div class="privacytextheader"><h4>Your e-mail address:</h4>
            <div class="privacytext"

            <p>1. We require your e-mail address when you register at this store.</p>

            <p>2. Your e-mail address will only be used to inform you about your order(s). </p>
            <p>3. We do not send you any advertisements unless you subscribe to our newsletter.</p>
            <p>4. When subscribed to our newsletter we will send you a newsletter on regular basis and inform you about interesting promotions.</p>
            <p>5. You can unsubscribe from the newsletter anytime. A single click unsubscribe link is at the bottom of each newsletter.</p>
            <p>6. Our newsletter is handled by Campaign monitor. They have a very strict privacy policy. When you subscribe to our newsletter your e-mail address will be stored in their secure database. They are prohibited to use your e-mail address or to sell it.</p>
        </div>

        <div class="privacytextheader"><h4>External parties:</h4>
            <div class="privacytext"

            <p>1. We have to share some of your information with third parties to process your orders. These parties are prohibited from using your personally identifiable information for promotional purposes. </p>

             </div>

        <div class="privacytextheader"><h4>Cookies:</h4>
            <div class="privacytext"

            <p>1. A cookie is a small text file on your computer accessible by a website. </p>
            <p>2. We use cookies to monitor website traffic and to create visitor statistics.</p>
            <p>3. We also use cookies to offer a persistent shopping experience. </p>
        </div>

        <div class="privacytextheader"><h4>Editting and deleting account information</h4>
            <div class="privacytext"

            <p>1. You can edit your account personal information when you log in and navigate to "Account information". </p>
            <p>2. You can request us by e-mail to remove or suspend your account. Because of anti-fraud measures we need to store your personal information for at least two years after your last credit card transaction. Invoices with personal information need to be stored for at least ten years to comply with local tax legislation. We will remove all other personal information anytime on your request.</p>

        </div>

        <div class="privacytextheader"><h4>Changes and updates:</h4>
            <div class="privacytext"
        <p>We may update this policy at any time in its sole discretion and without prior notice. We will strive to notify you about significant changes in the way we treat personal information by sending a notice to the primary email address specified in your store account and/or by placing a notice on the website.

            If you have questions or suggestions about our privacy policy you can contact us.</p>
        </div>
    </div>