
<div class="signupsignin">
    <form class= "signupform" class="form" name="signupForm" action="../Handlers/SignUpHandler.php" method="Post" onsubmit="">
        <fieldset>
            <legend><h1>Sign Up</h1></legend>

            <p>Please fill in this form to create an account.</p>


            <label for="email"><b>Email :</b></label>
            <input class="contact-form input" type="text" placeholder="Enter Email" onkeyup="mailvalidate()" id="mail" name="signup_email" required >
            <br>
            <label for="signup_psw"><b>Password :</b></label>
            <input class="contact-form input" type="password" placeholder="Enter Password" id="psw"  onkeyup="validatepassword()" name="signup_psw" required>
            <br>
            <label for="signup_pswrep"><b>Repeat Password :</b></label>
            <input class="contact-form input" type="password" placeholder="Repeat Password" id="pswcon" onkeyup="validatepassword()" name="signup_pswrep" required>
            <br>

            <p><input class="contact-form input" type="checkbox" name="terms" required=""></p><p>By creating an account you
                agree to our <a href="?page=terms" style="color:dodgerblue">Terms</a>/<a href="?page=privacy" style="color:dodgerblue">Privacy</a></p>
            <div class="clearfix">

                <input type="submit" name="submit_signup" class="signupbtn" id="signup" value="Sign Up"> </div>


        </fieldset>

    </form>

</div>
