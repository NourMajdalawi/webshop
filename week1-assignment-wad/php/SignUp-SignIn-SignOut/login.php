
<div class="signupsignin">
			<form class="loginform"class="form" action="../Handlers/LoginHandler.php" method="Post">
				<fieldset>
					<legend><h1>Log in</h1></legend>
					<p>Please fill in this form to log in.</p>
					<span style="color: darkred;"><?php if(isset($err)){echo $err;} ?></span><br>
					<label for="email">Email: </label><input class="form-control" placeholder="Enter e-mail" id="email" name="login_email" autofocus
					                                         required><br>
					<label for="password">Password: </label><input type="password" placeholder="Enter Password" class="form-control" id="password" name="login_password" required>
                    <br><br>
					<input class="contact-form input" type="submit" name="submit_login" value="Sign in">
                    <br><br><span><a href="./index.php?page=signup" style="color: #05abe0" >If you dont have an account, sign up here!</a></span>

				</fieldset>
			</form>
</div>