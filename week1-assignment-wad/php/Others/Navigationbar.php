
<div class="pageNavigation" >

	<nav class="tabs">

		<div class="logo">

			<img class="logo" href="?page=home" src="../Images/logo.png" alt="SinoTech Logo" />

		</div>

<div class="grid-container-navigation">
		<a class="home" href="?page=home">Home</a>
		<a class="aboutuscontactus" href="?page=AboutUsAndContactUs">About Us & Contact Form</a>

		<a class="categories" onmouseover="myFunction()">Categories</a>
		<div class="dropdown">

			<div id="myDropdown" class="dropdown-content">
				<a href="?page=laptop">Laptops</a>
				<a href="?page=smartphone">Smart Phones</a>
				<a href="?page=other">Other</a>
			</div>
		</div>

        <a class="memberpage" href="?page=memberPage">Member page </a>

		<?php  if(!isset($_SESSION['email'])){ ?>
			<a class="signupsigninnav" href="?page=login">Sign Up/Sign In</a>
		<?php } ?>

        <?php  if(isset($_SESSION['email'])){ ?>
			<a class="signupsigninnav" href="?page=logout">Log out</a>
		<?php } ?>

    <?php  if(isset($_SESSION['admin'])){ ?>
        <a class="admin" href="?page=admin">Admin</a>
    <?php } ?>

</div>
	</nav>

<div class="basket">
	<a href="?page=basket"> <img src="../Images/basket.jfif" alt="Shopping Basket" width="20px" height="15px"></a>
</div>
<div class="spacer"></div>
</div>