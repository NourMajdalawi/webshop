
<div class="logout-container">
<?php
if(!isset($_SESSION['email'])){
    die("<strong> log in first</strong>");
}
echo"Welcome " . $_SESSION["email"] . ".";
?>


<h1>Hello welcome to the member page  </h1><br>
    <div class="exclusive">
    <h3><strong>Exclusive page</strong></h3>
        <p>This page is special for all members of SinoTech. Only by owning an account on this website (and being logged in), this page will be available.</p>
        <p>We will try to keep you updated with our latest news and special promotions for members only!</p>
        <p>Members will always be noticed in advance with any promotion or special offer, so keep an eye on this page to be as up to date as possible.</p>
        <br>
        <h3><strong>Current member promotions</strong></h3>
        <li>By orders above €1000, 10% off. Use promocode: MEMBER10</li>
        <li>When buying an iphone 11, free airpods. Use promocode: MEMBERAIRPOD</li>
        <br>
        <h3><strong>Free shipment</strong></h3>
        <p>Our members are valuable to us. Therefore we offer them free shipment with every order.</p>
        <br>
        <button id="ajaxButton" class="MoreInfobutton">Show more data</button>
        <br>
        <p id="placeholder"></p>

        <script src="../JavaScript/ajax.js"></script>
    </div>

</div>
