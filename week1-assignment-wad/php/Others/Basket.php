<hr>
<h1 align="middle"> Shopping Basket</h1>


<hr>


<div class="grid-container-basket">
<div class="item">
        <div class="item1">
			<div class="product-image"><img src="../Images/iPhone11.jpg" width="120" height="100"
			                                alt=""/></div>
			<h4 class="product-name">Apple iphone 11</h4>
			<div class="price-box"><span class="price" id="product-price-1">€1500</span></div>
			<div class="actions">Quantity:<input type="number" name="sQ" min="1" max="10"></div>
        </div>
</div>
<div class="item">
        <div class="item2">
			<div class="product-image"><img src="../Images/playstation.jpg" width="120" height="100"
			                                alt="Microsoft Surfacebook 2"/></div>
			<h4 class="product-name">Playstation 4</h4>
			<div class="price-box"><span class="price" id="product-price-1">€3200</span></div>
			<div class="actions">Quantity:<input type="number" name="sQ" min="1" max="10"></div>
        </div>
</div>
<div class="item">
	<div class="item3">
            <div class="product-image"><img src="../Images/tv.jpg"  width="120" height="100"
                                            alt=""/></div>
            <h2 class="product-name">TV Sony</h2>
            <div class="price-box"><span class="price" id="product-price-1">€900</span></div>
            <div class="actions">Quantity:<input type="number" name="sQ" min="1" max="10"></div>
        </div>
</div>

	<div class="item">
        <div class="item4">
            <div class="product-image"><img src="../Images/samsung.png" width="120" height="100"
                                            alt=""/></div>
            <h4 class="product-name">Samsung note 10</h4>
            <div class="price-box"><span class="price" id="product-price-1">€700</span></div>
            <div class="actions">Quantity:<input type="number" name="sQ" min="1" max="10"></div>
        </div>

</div>
</div>

<div class="total-order inline">


		<div class="container">
			<h4>Total amount in cart:
				<span class="price" style="color:black">
								          <i class="fa fa-shopping-cart"></i>
								          <b></b>
								        </span>
			</h4>
			<p><a href="#">Apple Iphone 11: </a> <span class="price"> €1500</span></p>
			<p><a href="#">Playstation 4: </a> <span class="price"> €3200</span></p>
			<p><a href="#">TV Sony: </a> <span class="price"> €900</span></p>
			<p><a href="#">Samsung note 10: </a> <span class="price"> €700</span></p>
			<hr>
			<p>Total <span class="price" style="color:black"><b>€6300</b></span></p>
            <a href="http://www.ing.nl/" target="_blank">
                <button>Check out</button>
            </a>
		</div>
	</div>



<hr>




