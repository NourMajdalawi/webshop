<!--//make sure it is the same style-->

<link rel="shortcut icon" href="../../Images/icon.png" type="image/png">
<link rel="stylesheet"  href="../../Style/style.css" type="text/css">
<script src="../../JavaScript/script.js"></script>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create new product</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<?php
include "../../Handlers/CreateHandler.php";
?>

<body>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h2>Create Record</h2>
                </div>
                <p>Please fill this form and submit to add a new product to the database.</p>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                        <label>Name:</label>
                        <input type="text" name="name" class="form-control" value="<?php echo $name; ?>">
                        <span class="help-block"><?php echo $name_err;?></span>
                    </div>

                    <div class="form-group <?php echo (!empty($price_err)) ? 'has-error' : ''; ?>">
                        <label>Price:</label>
                        <input type="text" name="price" class="form-control" value="<?php echo $price; ?>">
                        <span class="help-block"><?php echo $price_err;?></span>
                    </div>
                    <div class="form-group <?php echo (!empty($info1_err)) ? 'has-error' : ''; ?>">
                        <label>Description:</label>
                        <input type="text" name="info1" class="form-control" value="<?php echo $info1; ?>">
                        <span class="help-block"><?php echo $info1_err;?></span>
                    </div>

                    <div class="form-group <?php echo (!empty($info2_err)) ? 'has-error' : ''; ?>">
                        <label>Feature1:</label>
                        <input type="text" name="info2" class="form-control" value="<?php echo $info2; ?>">
                        <span class="help-block"><?php echo $info2_err;?></span>
                    </div>

                    <div class="form-group <?php echo (!empty($info3_err)) ? 'has-error' : ''; ?>">
                        <label>Feature2:</label>
                        <input type="text" name="info3" class="form-control" value="<?php echo $info3; ?>">
                        <span class="help-block"><?php echo $info3_err;?></span>
                    </div>

                    <div class="form-group <?php echo (!empty($info4_err)) ? 'has-error' : ''; ?>">
                        <label>Feature3:</label>
                        <input type="text" name="info4" class="form-control" value="<?php echo $info4; ?>">
                        <span class="help-block"><?php echo $info4_err;?></span>
                    </div>

                    <div class="form-group <?php echo (!empty($category_err)) ? 'has-error' : ''; ?>">
                        <label>Category:</label>
                        <input type="text" name="category" class="form-control" value="<?php echo $category; ?>">
                        <span class="help-block"><?php echo $category_err;?></span>
                    </div>

                    <input type="submit" class="btn btn-primary" value="Submit">
                    <a href="../index.php?page=admin" class="btn btn-default">Cancel</a>
                </form>
            </div>
        </div>
    </div>
</div>
<?php


include "../../Models/Product.php";
$product = new Product();
$product->CreateProduct();
?>
</body>
</html>