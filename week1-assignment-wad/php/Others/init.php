<?php
spl_autoload_register('mvcAutoloader');
// the class' names should be same as file names
function mvcAutoloader ($className) {
    $fileName = '../Controllers/'. $className .'.php';

    if (!file_exists($fileName)) {
        $fileName = '../Models/'. $className .'.php';
        if (!file_exists($fileName)) {
            return false;
        }
    }

    include_once $fileName;
}
?>