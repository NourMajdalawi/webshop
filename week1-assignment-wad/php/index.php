<?php
session_start();
include_once "Others/init.php";
?>
<?php
echo "<html>";

/*To check weather it has a value or not? If it is set or not?*/
if( isset($_GET['page']) )
{
	$page = $_GET['page'];
}else
{
	//if the user typed any rash thing in the url field we will redirect him/her to the home page
	$page = "home";
}
include "Others/Head.php";
include "Others/Navigationbar.php";

switch($page) {
	case "home":
		include "HomePage/home.php";
		break;
	case "AboutUsAndContactUs":
		echo "<body>";
		include "AboutUsAndContact/AboutUsAndContact.php";
		break;
	case "memberPage":
        include "Others/memberPage.php";
        break;
	case "basket":
		include "Others/Basket.php";
		break;
    case "logout":
	    echo "<body>";
        include "SignUp-SignIn-SignOut/logout.php";
        break;
	case "login":
		include "SignUp-SignIn-SignOut/login.php";
		break;
    case "signup":
        include "SignUp-SignIn-SignOut/signUp.php";
        break;
    case "terms":
        include "Footer/terms.php";
        break;
    case "privacy":
        include "Footer/privacy.php";
        break;
    case "admin":
        include "Others/Admin.php";
        break;
    case "laptop":
        include "Others/Laptop.php";
        break;
    case "other":
        include "Others/other.php";
        break;
    case "smartphone":
        include "Others/Smartphone.php";
        break;

    case "create":
        include "Others/CreateProduct.php";
        break;

	default:
		include "HomePage/home.php";

}
require "Footer/Footer.php";
echo "</body>";
echo "</html>";

?>