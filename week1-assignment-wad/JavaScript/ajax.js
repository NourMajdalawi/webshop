class IndexPage {
    constructor() {

        let ajaxButton = document.getElementById('ajaxButton');
        ajaxButton.addEventListener('click', this.retrieveUsersViaAJAX);

    }
    retrieveUsersViaAJAX()
    {
        let xhrObject  = new XMLHttpRequest();
        // Process response
        xhrObject.onload  = function() {			//when readystate changes
            if (xhrObject.status == 200) {		//if server status was ok
                let articleTag = document.getElementById('placeholder');
                let userObjects = JSON.parse(xhrObject.response);
                userObjects.forEach(u => {
                    let pTag = document.createElement('p');
                    pTag.innerText = u.id + ' - '+ u.name;
                    articleTag.appendChild(pTag);
                });
            }
        };

        // Send the request
        xhrObject.open('GET', 'http://localhost/wad-assignments/week1-assignment-wad/data.json', true); 	//prepare the request
        xhrObject.send();
        document.getElementById('ajaxButton').disabled = true;
    }
    
}
let obj = new IndexPage();