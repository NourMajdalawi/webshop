//dropdown menu
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}
// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {

        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');

            }
        }
    }

}
//Form validation

/*This function the contact form fields not be empty and
if it is, give it a red border, alert the user and get
focused in the the empty field. It also make the border
to none when it becomes non empty
*/
function validateContactForm(){

    if(document.contactForm.Email.value==""){
        document.contactForm.Email.style.border ="3px dashed red";
        alert("Please provide your Email!");
        document.contactForm.Email.focus();
        return false;
    }else if(!validateEmail()){
        document.contactForm.Email.style.border ="3px dashed red";
        alert("Please enter the email in correct format of someone@example.domain");
        return false;
    }else{
        document.contactForm.Email.style.border ="none";
    }

    if(document.contactForm.firstname.value==""){
        document.contactForm.firstname.style.border ="3px dashed red";
        alert("Please provide your First Name!");
        document.contactForm.firstname.focus();
        return false;
    }else{
        document.contactForm.firstname.style.border ="none";
    }

    if(document.contactForm.lastname.value==""){
        document.contactForm.lastname.style.border ="3px dashed red";
        alert("Please provide your last name!");
        document.contactForm.lastname.focus();
        return false;
    }else{
        document.contactForm.lastname.style.border ="none";
    }

    if(document.contactForm.Telephone.value==""){
        document.contactForm.Telephone.style.border ="3px dashed red";
        alert("Please provide your telephone!");
        document.contactForm.Telephone.focus();
        return false;
    }else{
        document.contactForm.Telephone.style.border ="none";
    }

    if (document.contactForm.issue.value=="-1"){
        document.contactForm.issue.style.border ="3px dashed red";
        alert("Please provide your contact issue!");
        document.contactForm.issue.focus();
        return false;
    }else{
        document.contactForm.issue.style.border ="none";
    }
}

//Check the same thing for subscribe part
function validateSubsribe(){
    if(document.subsribe.SubEmail.value==""){
        document.subsribe.SubEmail.style.border ="3px dashed red";
        alert("Please provide your Email for subsribing!");
        document.subsribe.SubEmail.Email.focus();
        return false;
    }else if(!validateSubEmail()){
        document.subsribe.SubEmail.style.border ="3px dashed red";
        alert("Please enter the email in correct format of someone@example.domain");
        return false;
    }else{
        document.subsribe.SubEmail.style.border ="none";
    }
}

//if the email in subscribe form does not start with @ and it
//also has an @ and some domain comes after @
function validateSubEmail(){
    var emailID = document.subsribe.SubEmail.value;
    var atsignPos = emailID.indexOf("@");
    var dotsignPos = emailID.indexOf(".");

    if(atsignPos<1 ||(dotsignPos - atsignPos<2) ){
        return false
    }
    return (true);
}

/*
Check if the email in contact form does not start with @ and it
also has an @ and some domain comes after @
*/
function validateEmail() {
    var emailID = document.contactForm.Email.value;
    var atsignPos = emailID.indexOf("@");
    var dotsignPos = emailID.indexOf(".");

    if(atsignPos<1 ||(dotsignPos - atsignPos<2) ){
        return false
    }
    return (true);
}
function mailvalidate() {
    if(testmail()&& document.getElementById('mail').value.length>10){
        document.getElementById('mail').style.border="3px dashed green";}

    else{
        document.getElementById('mail').style.border="3px dashed red";}
}
function validatepassword(){
    var psw = document.getElementById('psw').value;
    var psw_repeat =document.getElementById('pswcon').value;
    if(psw != psw_repeat){
        document.signupForm.signup_psw.style.border="3px dashed red";
        document.signupForm.signup_pswrep.style.border="3px dashed red";
        document.getElementById('signup').disabled = true;



    } else {
        document.signupForm.signup_psw.style.border="3px dashed green";
        document.signupForm.signup_pswrep.style.border="3px dashed green";
        document.getElementById('signup').disabled = false;


    }
}

//Map function

function HideMap() {

    document.getElementById('Mapframe').style.display = 'none';
    document.getElementById('HideMapBtn').style.display = 'none';
    document.getElementById('ShowMapBtn').style.display = 'block';
}

function ShowMap() {
    document.getElementById('Mapframe').style.display = 'block';
    document.getElementById('HideMapBtn').style.display = 'block';
    document.getElementById('ShowMapBtn').style.display = 'none';
}
//Ajax
